#include <iostream>
#include <fcntl.h>
#include <unistd.h>
#include <vector>
#include <usb.h>
#include <string.h>

#include <iostream>
#include <algorithm>
#include <unistd.h>
#include <map>


#include "Thread.h"
#include "StringExtensions.h"
#include "CommandSwitchPads.h"
#include "CommandSwitchPad.h"
#include "liblegodimensionsportal.h"


using namespace std;

void PortalCallback(padEvent event);
bool ProcessKeyboardInput(string input);

int NextColourIndex = 0;
int MaxColourIndex = 14;
rgb Colours[14] = {
	rgb(255, 0, 0),
	rgb(255, 153, 0),
	rgb(255, 255, 0),
	rgb(0, 128, 0),	
	rgb(0, 0, 255),
	rgb(75, 0, 130),
	rgb(238, 130, 238),
	
	
	rgb(128, 0, 0),
	rgb(128, 76, 0),
	rgb(128, 128, 0),
	rgb(0, 64, 0),	
	rgb(0, 0, 128),
	rgb(37, 0, 65),
	rgb(119, 65, 119),
};


map<string, rgb> DeviceColours;


int main(int argc, char *argv[])
{
	
	InitializePortal();
	StartPadMonitor();
	RegisterPadEventCallback(PortalCallback);
	
	
	
		
	string input;
	while (true)
	{
		getline(cin, input);
		toUpper(input);
		
		if (!ProcessKeyboardInput(input))
		{
		
			break;
		}
	}
			
	StopPortal();
	

	
	
	return 0;
}



//  Process keyboard input from the board read / demo file run loop
//
bool ProcessKeyboardInput(string input)
{
	if (input.compare("Q") == 0) 
	{
		return false;
	}
	
	
	return true;
}

bool IndicatorPad = false;

void PortalCallback(padEvent event)
{
	cout << "Device " << event.Uid << " was " << (event.Removed ? " removed from pad " : " added to pad ") << event.Pad << endl; 
	
	string key = string(event.Uid);

	if (event.Removed)
	{
		PadLightOff(event.Pad);
		if (IndicatorPad)
		{
			PadLightOff(1);
			IndicatorPad = false;
		}
	}
	else
	{
		if (DeviceColours.find(key) == DeviceColours.end())
		{
			if (NextColourIndex == MaxColourIndex)
				NextColourIndex = 0;
		
			cout << "New device assigned colour " << NextColourIndex << "." << endl;
			DeviceColours[key] = Colours[NextColourIndex++];
			SwitchPadLight(1, rgb(0, 200, 0));
			IndicatorPad = true;
			SwitchPadLight(event.Pad, DeviceColours[key]);
		}
		else
		{
			SwitchPadLight(event.Pad, DeviceColours[key]);
		}
	}
		
}